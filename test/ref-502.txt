TestSweeper version NA, id NA
input: ../example --ref y foo5
                                    SLATE        SLATE        SLATE         Ref.         Ref.          
type       m       n       k        error    time (ms)      Gflop/s    time (ms)      Gflop/s  status  
   d     100     100     100   1.2346e-15  -----------  -----------  -----------  -----------  pass    
   d     200     200     200   2.4691e-15  -----------  -----------  -----------  -----------  pass    
   d     300     300     300   3.7037e-15  -----------  -----------  -----------  -----------  pass    
   d     400     400     400   4.9382e-15  -----------  -----------  -----------  -----------  pass    
   d     500     500     500   6.1728e-15  -----------  -----------  -----------  -----------  pass    
All tests passed.
