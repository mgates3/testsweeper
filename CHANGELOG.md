2020.09.00
  - CMake improvements to use as sub-project.

2020.06.00
  - Convert repo to git.

2020.03.00
  - Initial release.
